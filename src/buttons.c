/*
 * wmxmms2
 * Copyright (c)2005 Remy Bosch (remybosch@zonnet.nl)
 * 
 * This software covered by the GPL.  See COPYING file for details.
 *
 * buttons.c
 *
 */
#include "buttons.h"

int scrollNextPrev(const XEvent *event);
int scrollFwdRwnd(const XEvent *event);

/* geographic(-like) data */
struct coord {
	int x;
	int y;
	int w;
	int h;
};

/* Buttons when up - are at: */
struct coord btn_up[] = {
	{ 4, 95, 14, 11},       /* play */
	{18, 95, 14, 11},       /* pause*/
	{32, 95, 14, 11},       /* prev */
	{46, 95, 14, 11},       /* next */
	{ 4,110, 14, 11},       /* rwnd */
	{18,110, 14, 11},       /* fwd */
	{32,110, 14, 11},       /* reandom */
	{46,110, 14, 11}/* stop */
};

/* Buttons when down - are at: */
struct coord btn_down[] = {
	{ 4, 65, 14, 11},       /* play */
	{18, 65, 14, 11},       /* pause*/
	{32, 65, 14, 11},       /* prev */
	{46, 65, 14, 11},       /* next */
	{ 4, 80, 14, 11},       /*rwnd*/
	{18, 80, 14, 11},       /*fwd*/
	{32, 80, 14, 11},       /* random */
	{46, 80, 14, 11}/* stop */
};

/* Buttons are to be placed at: */
struct coord btn_pos[] = {
	{ 4, 34, 14, 11},       /* play */
	{18, 34, 14, 11},       /* pause*/
	{32, 34, 14, 11},       /* prev */
	{46, 34, 14, 11},       /* next */
	{ 4, 49, 14, 11},       /*rwnd*/
	{18, 49, 14, 11},       /*fwd*/
	{32, 49, 14, 11},       /* random */
	{46, 49, 14, 11}/* stop */
};

/**
 * Place pressed button
 */
void buttonDown(int i){
	copyXPMArea(btn_down[i].x, btn_down[i].y, btn_down[i].w, btn_down[i].h,
			btn_pos[i].x, btn_pos[i].y);
	#ifdef _DEBUG_
	printf("Button %d DOWN\n",i);
	#endif
}

/**
 * Place normal button
 */
void buttonUp(int i){
	copyXPMArea(btn_up[i].x, btn_up[i].y, btn_up[i].w, btn_up[i].h,
			btn_pos[i].x, btn_pos[i].y);
	#ifdef _DEBUG_
	printf("Button %d UP\n",i);
	#endif
}

/**
 * Place normal buttons
 */
void initButtons (void){
	int i;
	i = 0;
	for (i = 0; i < MAX_MOUSE_REGION ; i++){
		AddMouseRegion(i, btn_pos[i].x, btn_pos[i].y,
			btn_pos[i].x + btn_pos[i].w,
			btn_pos[i].y + btn_pos[i].h);
		copyXPMArea(btn_up[i].x, btn_up[i].y, btn_up[i].w, btn_up[i].h,
					btn_pos[i].x, btn_pos[i].y);
	}
}

/**
 * What to do whe a button is pressed?
 */
int buttonPressHandler(const XEvent *Event){
	int butnum;
	butnum = CheckMouseRegion(Event->xbutton.x,Event->xbutton.y);
	#ifdef _DEBUG_
	printf ("Button %d Pressed\n",butnum);
	#endif
	switch(butnum){
	case PLAY:
		buttonDown(PLAY);
		buttonUp(PAUSE);
		play_cmd();
		break;
	case PAUSE:
		buttonDown(PAUSE);
		buttonUp(PLAY);
		pause_cmd();
		break;				
	case RANDOM:
		buttonDown(RANDOM);
		randomize_cmd();
		break;
	case PREV:
		if (!scrollNextPrev(Event)){
		buttonDown(PREV);
		prev_cmd();
		}
		break;
	case NEXT:
		if (!scrollNextPrev(Event)){
		buttonDown(NEXT);
		next_cmd();
		}
		break;
	case RWND:
		if (!scrollFwdRwnd(Event)){
		buttonDown(RWND);
		rwnd_cmd();
		}
		break;
	case FWD:
		if (!scrollFwdRwnd(Event)){
		buttonDown(FWD);
		fwd_cmd();
		}
		break;
	case STOP:
		buttonDown(STOP);
		buttonUp(PAUSE);
		buttonUp(PLAY);
		stop_cmd();
		break;
	default :
		if ( butnum != -1 )
			fprintf(stderr,"How is this possible? : %d\n",butnum);
	}
	RedrawWindow(); 
	return(butnum);
}

/**
 * See if the scrollbutons are used...
 */
int scrollNextPrev(const XEvent *event){
	if (event->xbutton.button == BUTTON_WHEEL_UP){
		next_cmd();
		return 1;
	}else if (event->xbutton.button == BUTTON_WHEEL_DOWN){
		prev_cmd();
		return 1;
	}else{
		return 0;
	}
}

/**
 * See if the scrollbutons are used...
 */
int scrollFwdRwnd(const XEvent *event){
	if (event->xbutton.button == BUTTON_WHEEL_UP){
		fwd_cmd();
		return 1;
	}else if (event->xbutton.button == BUTTON_WHEEL_DOWN){
		rwnd_cmd();
		return 1;
	}else{
		return 0;
	}
}

/**
 * If something has changed than react accordingly
 */
void stateUpdate(unsigned int NewState){
	static unsigned int OldState=99;
	if (NewState!=OldState)
	switch(STATE){
	case S_PLAY:
		buttonDown(PLAY);
		buttonUp(PAUSE);
		break;
	case S_PAUSE:
		buttonDown(PAUSE);
		buttonUp(PLAY);
		break;				
	default :
		buttonUp(PLAY);
		buttonUp(PAUSE);
	}
	RedrawWindow();
	OldState=NewState;
}

/**
 * Oh.. my... The button is released... What to do now?
 */
void buttonReleaseHandler(int butnum){
	if ((butnum == RANDOM) || (butnum == PREV) ||
		(butnum == RWND) || (butnum == FWD) ||
		(butnum == NEXT) || (butnum == STOP)){
		usleep(50000);
		buttonUp(butnum);
		RedrawWindow();
	}
}

