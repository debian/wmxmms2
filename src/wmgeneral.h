/*
 * wmxmms2
 * Copyright (c)2005 Remy Bosch (remybosch@zonnet.nl)
 * 
 * This software covered by the GPL.  See COPYING file for details.
 *
 * wmgeneral.h
 *
 */
#ifndef WMGENERAL_H_INCLUDED
#define WMGENERAL_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/xpm.h>
#include <X11/extensions/shape.h>
#define DISPLAY_LEN 56
#define MAX_MOUSE_REGION 8
#define FONT_HEIGHT 10
#define FONT_WIDTH 8
#define NUM_CHARS (DISPLAY_LEN/FONT_WIDTH)
/* Progressbar */
#define RED 0
#define GREEN 1
#define BLACK 2

typedef struct _rckeys rckeys;

struct _rckeys {
	const char	*label;
	char		**var;
};

typedef struct _rckeys2 rckeys2;

struct _rckeys2 {
	const char	*family;
	const char	*label;
	char		**var;
};

typedef struct {
	Pixmap			pixmap;
	Pixmap			mask;
	XpmAttributes	attributes;
} XpmIcon;


Display		*display;
Window          Root, iconwin, win;


void AddMouseRegion(int index, int left, int top, int right, int bottom);
int CheckMouseRegion(int x, int y);

void openXwindow(int argc, char *argv[], char **, char *, int, int);
void RedrawWindow(void);
void RedrawWindowXY(int x, int y);

void createXBMfromXPM(char *, char **, int, int);
void copyXPMArea(int, int, int, int, int, int);
void copyXBMArea(int, int, int, int, int, int);
void setMaskXY(int, int);

void parse_rcfile(const char *, rckeys *);
void fontColorInit(const char *colorOption);
void drawChar(char c, int x, int y);
void drawString(char *s, int x, int y);
void drawPartString(char *s, int x, int y, unsigned int s_begin, unsigned int s_end );
void scroller(char *s, int x, int y);
void lineDraw(unsigned int color, int x,int y,unsigned int length);
#endif
